package com.example.retrofit4.api

data class ApiData(
    val fileSizeBytes: Int,
    val url: String)